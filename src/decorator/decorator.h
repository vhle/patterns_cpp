#include <algorithm>
#include <array>
#include <memory>
#include <string>

namespace patterns_cpp {
namespace decorator {

struct Component {
  virtual std::string get() const = 0;
};

class Decorator : public Component {
 public:
  explicit Decorator(std::unique_ptr<Component>&& c)
      : mComponent(std::move(c)) {}

 protected:
  std::unique_ptr<Component> mComponent;
};

struct Coffee : public Component {
  std::string get() const override { return "coffee"; }
};

class AddMilk : public Decorator {
 public:
  using Decorator::Decorator;
  std::string get() const override { return mComponent->get() + " with milk"; }
};

class AddSugar : public Decorator {
 public:
  using Decorator::Decorator;
  std::string get() const override { return mComponent->get() + " with sugar"; }
};

class AddSoybean : public Decorator {
 public:
  using Decorator::Decorator;
  std::string get() const override {
    return mComponent->get() + " with soybean";
  }
};

}  // namespace decorator
}  // namespace patterns_cpp

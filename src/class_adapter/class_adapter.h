#include <string>

namespace patterns_cpp {
namespace class_adapter {

struct Target {
  virtual std::string request() const = 0;
};

struct Adaptee {
  std::string specificRequest() const { return "Specific request"; }
};

struct Adapter : public Target, public Adaptee {
  Adapter() = default;
  std::string request() const override { return specificRequest(); }
};
}  // namespace class_adapter
}  // namespace patterns_cpp

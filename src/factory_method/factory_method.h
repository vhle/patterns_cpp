#include <memory>
#include <string>

namespace patterns_cpp {

struct Product {
  virtual std::string getName() const = 0;
};

struct ConcreteProduct : public Product {
  std::string getName() const override { return "Concrete product"; }
};

struct Creator {
  virtual std::shared_ptr<Product> makeProduct() const = 0;
  std::string makeAndPrintProduct() {
    auto product = makeProduct();
    return product->getName();
  }
};

struct ConcreteCreator : public Creator {
  std::shared_ptr<Product> makeProduct() const override {
    return std::make_shared<ConcreteProduct>();
  }
};

}  // namespace patterns_cpp

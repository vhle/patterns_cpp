#include <memory>
#include <string>
namespace patterns_cpp {
namespace object_adapter {

struct Target {
  virtual std::string request() const = 0;
};

struct Adaptee {
  std::string specificRequest() const { return "specific request"; }
};

struct Adapter : public Target {
  explicit Adapter(std::shared_ptr<Adaptee> adaptee) : mAdaptee(adaptee) {}
  std::string request() const override { return mAdaptee->specificRequest(); }

 private:
  std::shared_ptr<Adaptee> mAdaptee;
};

}  // namespace object_adapter
}  // namespace patterns_cpp

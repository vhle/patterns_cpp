#include <memory>
#include <string>
#include <vector>
namespace patterns_cpp {
namespace composite {

class Composite;
class Component {
 public:
  virtual void Operation(std::string&) = 0;
  virtual const Composite* getComposite() const { return nullptr; }
  virtual void add(std::shared_ptr<Component>) {}
  virtual void remove(std::shared_ptr<Component>) {}
  virtual size_t childCount() const { return 0; }
  virtual std::shared_ptr<Component> getChild(size_t) const { return nullptr; }
  std::shared_ptr<Component> getParent() const { return mParent; }

 protected:
  std::shared_ptr<Component> mParent;
};

class Composite : public Component {
  void Operation(std::string& out) override {
    for (auto c : mChildren) {
      c->Operation(out);
    }
  }

  const Composite* getComposite() const override { return this; }
  void add(std::shared_ptr<Component> c) override { mChildren.push_back(c); }
  void remove(std::shared_ptr<Component> c) override {
    for (auto it = std::begin(mChildren); it != std::end(mChildren); ++it) {
      if (*it == c) {
        mChildren.erase(it);
        return;
      }
    }
  }

  size_t childCount() const override { return mChildren.size(); }
  std::shared_ptr<Component> getChild(size_t pos) const override {
    return mChildren[pos];
  }

 private:
  std::vector<std::shared_ptr<Component>> mChildren;
};

class Leaf1 : public Component {
 public:
  void Operation(std::string& out) override { out += "Leaf1"; }
};

class Leaf2 : public Component {
 public:
  void Operation(std::string& out) override { out += "Leaf2"; }
};

}  // namespace composite
}  // namespace patterns_cpp

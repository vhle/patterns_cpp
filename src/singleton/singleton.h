#include <memory>

#include <iostream>
namespace patterns_cpp {

class Singleton {
public:
  static Singleton &getInstance() {
      static Singleton _instance;
      return _instance;
  }

  Singleton(const Singleton&) = delete ;
  Singleton(Singleton&&) = delete ;

private:
  Singleton(){};
};
}

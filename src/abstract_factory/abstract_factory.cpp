#include "abstract_factory.h"

namespace abstract_factory {
std::string usage(const AbstractFactory &factory) {
  auto a = factory.createProductA();
  auto b = factory.createProductB();
  return a->getName() + " " + b->getName();
}

} // namespace abstract_factory

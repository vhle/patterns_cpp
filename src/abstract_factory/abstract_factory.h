#pragma once
#include <string>
#include <memory>

namespace abstract_factory {


class AbstractProductA {
public:
  virtual std::string getName() = 0;
};

class AbstractProductB {
public:
  virtual std::string getName() = 0;
};

class ProductA1 : public AbstractProductA {
public:
  std::string getName() override { return "ProductA1"; }
};

class ProductA2 : public AbstractProductA {
public:
  std::string getName() override { return "ProductA2"; }
};

class ProductB1 : public AbstractProductB {
public:
  std::string getName() override { return "ProductB1"; }
};

class ProductB2 : public AbstractProductB {
public:
  std::string getName() override { return "ProductB2"; }
};

class AbstractFactory {
public:
  virtual std::unique_ptr<AbstractProductA> createProductA() const = 0;
  virtual std::unique_ptr<AbstractProductB> createProductB() const = 0;
};

class ConcreteFactory1: public AbstractFactory {
public:
  std::unique_ptr<AbstractProductA> createProductA() const override {
    return std::make_unique<ProductA1>();
  }

  std::unique_ptr<AbstractProductB> createProductB() const override {
    return std::make_unique<ProductB1>();
  }
};


class ConcreteFactory2: public AbstractFactory {
public:
  std::unique_ptr<AbstractProductA> createProductA() const override {
    return std::make_unique<ProductA2>();
  }

  std::unique_ptr<AbstractProductB> createProductB() const override {
    return std::make_unique<ProductB2>();
  }
};

std::string usage(const AbstractFactory &factory);
}

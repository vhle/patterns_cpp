#include <memory>
#include "composite/composite.h"
#include "gtest/gtest.h"

using namespace patterns_cpp::composite;
TEST(TestCompositePattern, UseCompositePattern) {
  /**
r0 ---r1 ---r2
   ---r3----r4
        |---r5
   */
  std::shared_ptr<Component> r0 = std::make_shared<Composite>();
  std::shared_ptr<Component> r1 = std::make_shared<Composite>();
  std::shared_ptr<Component> r2 = std::make_shared<Leaf1>();
  std::shared_ptr<Component> r3 = std::make_shared<Composite>();
  std::shared_ptr<Component> r4 = std::make_shared<Leaf1>();
  std::shared_ptr<Component> r5 = std::make_shared<Leaf2>();

  r0->add(r1);
  r0->add(r3);
  r1->add(r2);
  r3->add(r4);
  r3->add(r5);

  std::string s;
  r0->Operation(s);
  ASSERT_EQ("Leaf1Leaf1Leaf2", s);
}

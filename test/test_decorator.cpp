#include <memory>
#include <type_traits>
#include "decorator/decorator.h"
#include "gtest/gtest.h"

using namespace patterns_cpp::decorator;

TEST(TestDecorator, UseDecorators) {
  std::unique_ptr<Component> coffee = std::make_unique<Coffee>();
  std::unique_ptr<Component> withSugar =
      std::make_unique<AddSugar>(std::move(coffee));
  std::unique_ptr<Component> withMilk =
      std::make_unique<AddMilk>(std::move(withSugar));
  std::unique_ptr<Component> withSoybean =
      std::make_unique<AddSoybean>(std::move(withMilk));
  ASSERT_EQ("coffee with sugar with milk with soybean", withSoybean->get());
}

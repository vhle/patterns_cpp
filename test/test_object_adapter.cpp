#include "gtest/gtest.h"
#include "object_adapter/object_adapter.h"

using namespace patterns_cpp::object_adapter;

TEST(ObjectAdapterTest, UseObjectAdapter) {
  auto adaptee = std::make_shared<Adaptee>();
  std::shared_ptr<Target> target = std::make_shared<Adapter>(adaptee);
  ASSERT_EQ(target->request(), adaptee->specificRequest());
}

#include "class_adapter/class_adapter.h"
#include "gtest/gtest.h"

using namespace patterns_cpp::class_adapter;

TEST(ClassAdapterTest, UseClassAdapter) {
  Adapter ter;
  Adaptee tee;
  ASSERT_EQ(tee.specificRequest(), ter.request());
}

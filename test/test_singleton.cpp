#include "singleton/singleton.h"
#include "gtest/gtest.h"

using namespace patterns_cpp;

TEST(SingletonTest, UseSingleton) {
  auto& s1 = Singleton::getInstance();
  auto& s2 = Singleton::getInstance();
  ASSERT_EQ(&s1, &s2);
}

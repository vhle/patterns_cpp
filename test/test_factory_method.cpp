#include "gtest/gtest.h"

#include "factory_method/factory_method.h"

TEST(FactoryMethodTest, TestUsage) {
  auto c = patterns_cpp::ConcreteCreator();
  auto s = c.makeAndPrintProduct();
  auto anotherProduct = patterns_cpp::ConcreteProduct();

  ASSERT_EQ(s, anotherProduct.getName());
}

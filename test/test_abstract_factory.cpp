#include "abstract_factory/abstract_factory.h"
#include "gtest/gtest.h"

using namespace abstract_factory;
TEST(AbstractFactoryTest, Usage) {
  auto f1 = ConcreteFactory1();
  auto v1 = usage(f1);
  ASSERT_EQ("ProductA1 ProductB1", v1);

  auto f2 = ConcreteFactory2();
  auto v2 = usage(f2);
  ASSERT_EQ("ProductA2 ProductB2", v2);
}
